#### Build

```
export DATALAD_VER=0.13.5
docker build -t registry.git.mpib-berlin.mpg.de/wittkuhn/highspeed/datalad:$DATALAD_VER --build-arg DOCKER_TAG=$DATALAD_VER .
```

Based on:

https://hub.docker.com/r/markiewicz/datalad/dockerfile
https://github.com/datalad/datalad/blob/master/tools/Dockerfile.fullmaster
A couple of Dockerfiles to help speed up gitlab-ci.

#### Gitlab container registry

You can push container images to any namespace or repo that you wish. It makes
sense to re-use the namespace for a project for containers that are used in
this project.

To push images we have to login (once). Note that the credentials will be
stored unencrypted in your home. Only do this on trusted hosts.

```
docker login registry.git.mpib-berlin.mpg.de
```

#### Build and publish

```
cd bookdown
docker build -t registry.git.mpib-berlin.mpg.de/wittkuhn/highspeed/bookdown:latest .
docker push registry.git.mpib-berlin.mpg.de/wittkuhn/highspeed/bookdown:latest
```

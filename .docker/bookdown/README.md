### Build

```
docker build -t registry.git.mpib-berlin.mpg.de/wittkuhn/highspeed/bookdown:latest .
```

### Notes

If on two consecutives builds the Dockerfile didn't change up to a specific
line then docker can re-use previously built layers for each `RUN`-line. This
is extremely useful when developing a container and the reason I split the
large `install.packages()` list into multiple calls.

Also we want to stop after a `RUN` if it fails and that requires a little
wrapper around `install.packages()`.

Lastly, by setting the `Ncpus` option to 4 we can resolve up to 4 packages in
parallel.

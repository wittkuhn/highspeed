stages:
  - retrieve
  - publish

datalad:
  stage: retrieve
  image:
    name: registry.git.mpib-berlin.mpg.de/wittkuhn/highspeed/datalad:0.13.5
    entrypoint: [""]
  script:
    # rclone configurations:
    - rclone config create highspeed-bids seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library highspeed-bids pass $CI_KEEPER_PASS
    - rclone config create highspeed-analysis seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library highspeed-analysis pass $CI_KEEPER_PASS
    #- rclone config create highspeed-fmriprep seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library highspeed-fmriprep pass $CI_KEEPER_PASS
    - rclone config create highspeed-mriqc seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library highspeed-mriqc pass $CI_KEEPER_PASS
    - rclone config create highspeed-masks seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library highspeed-masks pass $CI_KEEPER_PASS
    - rclone config create highspeed-glm seafile url https://keeper.mpdl.mpg.de/ user wittkuhn@mpib-berlin.mpg.de library highspeed-glm pass $CI_KEEPER_PASS
    # git configuration:
    - git config --global user.name "GitLab CI"
    - git config --global user.email "ci@git.mpib-berlin.mpg.de"
    # remove and install all datasets:
    - datalad remove --dataset . bibliography
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/bibliography.git
    #- datalad remove --dataset . highspeed-analysis
    - rm -rf highspeed-analysis
    - datalad install --dataset . -r https://github.com/lnnrtwttkhn/highspeed-analysis.git
    - datalad remove --dataset . highspeed-bids
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/highspeed-bids.git
    - datalad remove --dataset . highspeed-fmriprep
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/highspeed-fmriprep.git
    - datalad remove --dataset . highspeed-mriqc
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/highspeed-mriqc.git
    - datalad remove --dataset . highspeed-masks
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/highspeed-masks.git
    - datalad remove --dataset . highspeed-glm
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/highspeed-glm.git
    - datalad remove --dataset . highspeed-decoding
    - datalad clone --dataset . https://github.com/lnnrtwttkhn/highspeed-decoding.git
    # add gin siblings for all datasets:
    - datalad siblings add --dataset highspeed-bids -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-bids
    - datalad siblings add --dataset highspeed-analysis -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-analysis
    - datalad siblings add --dataset highspeed-analysis/data/bids -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-bids
    - datalad siblings add --dataset highspeed-analysis/data/decoding -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-decoding
    #- datalad siblings add --dataset highspeed-fmriprep -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-fmriprep
    - datalad siblings add --dataset highspeed-mriqc -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-mriqc
    - datalad siblings add --dataset highspeed-masks -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-masks
    - datalad siblings add --dataset highspeed-glm -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-glm
    - datalad siblings add --dataset highspeed-decoding -s gin --url https://gin.g-node.org/lnnrtwttkhn/highspeed-decoding
    # enable keeper siblings for all datasets:
    - datalad siblings --dataset highspeed-analysis enable --name keeper
    - datalad siblings --dataset highspeed-bids enable --name keeper
    - datalad siblings --dataset highspeed-analysis/data/bids enable --name keeper
    #- datalad siblings --dataset highspeed-fmriprep enable --name keeper
    - datalad siblings --dataset highspeed-mriqc enable --name keeper
    - datalad siblings --dataset highspeed-masks enable --name keeper
    - datalad siblings --dataset highspeed-glm enable --name keeper
    # configure gin and keeper siblings for all datasets:
    - datalad siblings --dataset highspeed-bids configure --name origin --publish-depends gin --publish-depends keeper
    - datalad siblings --dataset highspeed-analysis configure --name origin --publish-depends gin
    - datalad siblings --dataset highspeed-analysis/data/bids configure --name origin --publish-depends gin --publish-depends keeper
    #- datalad siblings --dataset highspeed-fmriprep configure --name origin --publish-depends gin --publish-depends keeper
    - datalad siblings --dataset highspeed-mriqc configure --name origin --publish-depends gin --publish-depends keeper
    - datalad siblings --dataset highspeed-masks configure --name origin --publish-depends gin --publish-depends keeper
    - datalad siblings --dataset highspeed-glm configure --name origin --publish-depends gin --publish-depends keeper
    - datalad siblings --dataset highspeed-decoding configure --name origin --publish-depends gin
    - datalad siblings --dataset highspeed-analysis/data/decoding configure --name origin --publish-depends gin
    # get data
    - datalad get highspeed-analysis/data/bids/participants.tsv
    - datalad get highspeed-analysis/data/bids/sub-*/ses-*/func/*events.tsv || true
    - datalad get highspeed-analysis/data/bids/sub-*/ses-*/func/*events.tsv
    - datalad get highspeed-analysis/data/decoding/decoding/sub-*/data/*_decoding.csv || true
    - datalad get highspeed-analysis/data/decoding/decoding/sub-*/data/*_decoding.csv
    - datalad get highspeed-analysis/data/decoding/decoding/*/data/*thresholding.csv || true
    - datalad get highspeed-analysis/data/decoding/decoding/*/data/*thresholding.csv
    - datalad get highspeed-analysis/data/tmp/dt_pred_conc_slope.Rdata
  artifacts:
    paths:
      - bibliography/code/bibliography.bib
      - highspeed-bids/code
      - highspeed-analysis/code
      - highspeed-analysis/figures
      - highspeed-analysis/sourcedata
      - highspeed-analysis/data/bids/sub-*/ses-*/func/*events.tsv
      - highspeed-analysis/data/decoding/decoding/sub-*/data/*_decoding.csv
      - highspeed-analysis/data/decoding/decoding/sub-*/data/*_thresholding.csv
      - highspeed-analysis/data/tmp
      - highspeed-fmriprep/code
      - highspeed-mriqc/code
      - highspeed-glm/code
      - highspeed-decoding/code
    expire_in: 1 hour
  only:
    - master

pages:
  stage: publish
  image: registry.git.mpib-berlin.mpg.de/wittkuhn/highspeed/bookdown:latest
  script:
    - Rscript -e "options(bookdown.render.file_scope = FALSE); bookdown::render_book('index.Rmd', 'all', output_dir = 'public')"
  artifacts:
    paths:
      - public
      - highspeed-analysis/figures
      - highspeed-analysis/sourcedata
  only:
    - master

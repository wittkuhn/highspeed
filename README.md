# Faster than thought: Detecting sub-second activation sequences with sequential fMRI pattern analysis - Main repository

[![pipeline status](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed/badges/master/pipeline.svg)](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed/-/commits/master)

This repository combines all resources (mainly data and code) published along the paper *"Faster than thought: Detecting sub-second activation sequences with sequential fMRI pattern analysis"* by Wittkuhn & Schuck, 2020, *Nature Communications*.

Please go to **[https://wittkuhn.mpib.berlin/highspeed/](https://wittkuhn.mpib.berlin/highspeed/)** (the study's nickname is *highspeed*) to see the accompanying project website.

## Overview

Briefly, the published material include:
- [BIDS-converted and defaced MRI dataset](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed-bids/)
- [MRIQC quality metrics of BIDS-converted MRI data](hhttps://git.mpib-berlin.mpg.de/wittkuhn/highspeed-mriqc/)
- [Pre-processed BID-converted MRI data using fMRIPrep](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed-fmriprep/)
- [Bibliography](https://github.com/lnnrtwttkhn/bibliography/)

## Citation

> Wittkuhn, L. and Schuck, N. W. (2020). Faster than thought: Detecting sub-second activation sequences with sequential fMRI pattern analysis. *bioRxiv*. [doi:10.1101/2020.02.15.950667](http://dx.doi.org/10.1101/2020.02.15.950667)

## Contact

Please [create a new issue](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed/-/issues/new) if you have questions about the code or data, if there is anything missing, not working or broken. If there is a problem with any of the references subdataset, please open an issue in the relevant subdataset.

For all other general questions, you may also write an email to:

- [Lennart Wittkuhn](mailto:wittkuhn@mpib-berlin.mpg.de)
- [Nicolas W. Schuck](mailto:schuck@mpib-berlin.mpg.de)

## License

All custom-written code is licensed under the MIT license.
Please see the [LICENSE](LICENSE) for further details and cite the original paper (please see the [Citation](#citation) section) if you use (parts of) the code and data published here.

Please note, that subdatasets / submodules of this repository may have different licenses.
Further, code and data from third-parties, e.g., software like containers (e.g., fMRIPrep, MRIQC, etc.) may have different licenses.

All in all, properly licensing mixed code and data from different sources is tough so please be mindful, respect and credit the work of others and ask if you are unsure about the appropriate referencing of sources. Thanks!

---
title: "Dynamics of fMRI patterns reflect sub-second activation sequences and reveal replay in human visual cortex"
subtitle: "Project website with data and code"
author:
  - Lennart Wittkuhn ^[Max Planck Research Group "Neural and Computational Basis of Learning, Decision Making and Memory" - NeuroCode, Max Planck Institute for Human Development, Max Planck UCL Centre for Computational Psychiatry and Ageing Research wittkuhn@mpib-berlin.mpg.de]
  - Nicolas W. Schuck ^[Max Planck Research Group "Neural and Computational Basis of Learning, Decision Making and Memory - NeuroCode, Max Planck Institute for Human Development, Max Planck UCL Centre for Computational Psychiatry and Ageing Research, schuck@mpib-berlin.mpg.de]
date: "Last update: `r format(Sys.time(), '%d %B, %Y')`"
site: bookdown::bookdown_site
url: "https://wittkuhn.mpib.berlin/highspeed/"
description: "This is the project website of the accompanying the paper 'Faster than thought: Detecting sub-second activation sequences with sequential fMRI pattern analysis' by Wittkuhn & Schuck, 2020, Nature Communications"
header-includes:
  - \usepackage{fontspec}
  - \setmainfont{AgfaRotisSansSerif}
email: wittkuhn@mpib-berlin.mpg.de
documentclass: book
bibliography: bibliography/code/bibliography.bib
biblio-style: apal
link-citations: yes
fig.align: "center"
---

# Overview

## About this website

Welcome to the **project website for data and code**  accompanying the paper "*Dynamics of fMRI patterns reflect sub-second activation sequences and reveal replay in human visual cortex*" by Lennart Wittkuhn and Nicolas W. Schuck, 2020, *Nature Communications*!

This project website contains further details about the study, including *all* **data and code**^[We note that transparency and reproducibility of the results could still be further improved, e.g., by reporting the computational environments in more detail or specifying the link between code and data more explicitly. If you have any suggestions for further improvement, are missing information, or spotted a bug, we invite you to [create a new issue](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) or [send an email](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed/#contact)] that were used to generate the results reported in the paper.
A more detailed overview of the project structure and the associated data and code will be provided in the next sections.

Briefly, we publicly share the following code and data:

| Dataset                   | Contents                                                                                                                   | Links                                                                                                                                                   | Dependencies                                                                                                |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| `highspeed-analysis`      | all statistical analyses and code to reproduce the figures in the paper                                                    | [GitHub](https://github.com/lnnrtwttkhn/highspeed-analysis)                                                                                             | `highspeed-bids`, `highspeed-decoding`                                                                      |
| `highspeed-decoding`      | decoding results of our multivariate pattern analysis                                                                      | [GitHub](https://github.com/lnnrtwttkhn/highspeed-decoding), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-decoding)                               | `highspeed-bids`, `highspeed-fmriprep`, `highspeed-glm`, `highspeed-masks`                                  |
| `highspeed-task`          | all code to run the behavioral task used in the study                                                                      | [GitHub](https://github.com/lnnrtwttkhn/highspeed-task), [![DOI](https://zenodo.org/badge/310215989.svg)](https://zenodo.org/badge/latestdoi/310215989) | none                                                                                                        |
| `highspeed-bids`          | MRI data adhering to the [Brain Imaging Data Structure (BIDS)](https://bids.neuroimaging.io/)                              | [GitHub](https://github.com/lnnrtwttkhn/highspeed-bids), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-bids)                                       | `highspeed-data-behavior` and the acquired MRI DICOM data (that we can't share for data protection reasons) |
| `highspeed-glm`           | first-level GLM analysis used for feature selection in `highspeed-decoding`                                                | [GitHub](https://github.com/lnnrtwttkhn/highspeed-glm), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-glm)                                         | `highspeed-bids`, `highspeed-fmriprep`                                                                      |
| `highspeed-masks`         | binarized anatomical masks used for feature selection in `highspeed-decoding`                                              | [GitHub](https://github.com/lnnrtwttkhn/highspeed-masks), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-masks)                                     | `highspeed-bids`, `highspeed-fmriprep`                                                                      |
| `highspeed-fmriprep`      | pre-processed MRI data using [fMRIPrep](https://fmriprep.org/en/stable/)                                                   | [GitHub](https://github.com/lnnrtwttkhn/highspeed-fmriprep), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-fmriprep)                               | `highspeed-bids`                                                                                            |
| `highspeed-data-behavior` | un-processed behavioral task data acquired during the study using the task code in `highspeed-task`                        | [GitHub](https://github.com/lnnrtwttkhn/highspeed-data-behavior), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-data-behavior)                     | `highspeed-task`                                                                                            |
| `highspeed-mriqc`         | quality metrics and visual reports of the BIDS MRI data created using [MRIQC](https://mriqc.readthedocs.io/en/stable/)     | [GitHub](https://github.com/lnnrtwttkhn/highspeed-mriqc), [GIN](https://gin.g-node.org/lnnrtwttkhn/highspeed-mriqc)                                     | `highspeed-bids`, `highspeed-decoding`                                                                      |
| `bibliography`            | project-unspecific repository that contains the `.bib` bibliography file and some LaTeX commands used for document styling | [GitHub](https://github.com/lnnrtwttkhn/bibliography)                                                                                                   | none                                                                                                        |
| `tools`                   | project-unspecific DataLad dataset containing several containers and other useful computational tools                      | [GitHub](https://github.com/lnnrtwttkhn/tools)                                                                                                          | see repo                                                                                                    |

## About the code and data

### Dataset structure

Here, we describe the **structure of the code and data** published in several separate datasets using [DataLad](https://www.datalad.org/).
This website is build using [`bookdown`](https://bookdown.org/) based on contents stored in the top-level `highspeed` repository, publicly available at <https://git.mpib-berlin.mpg.de/wittkuhn/highspeed/>.
It mainly functions as the central repository combining all other subdatasets listed above.

The organization of most datasets follows the DataLad YODA principles.
Please read about YODA principles in the chapter ["YODA: Best practices for data analyses in a dataset" in the fantastic DataLad handbook](https://handbook.datalad.org/en/latest/basics/101-127-yoda.html).
Briefly, each dataset contains a `/code` directory that contains all custom-code used to create the content of this dataset.
Usually, each dataset is dependent on another dataset.
For example, the pre-processed MRI data in `highspeed-fmriprep` is based on the BIDS-structured MRI data in `highspeed-bids` and hence includes this repository as an input subdatatset.
This nesting of datasets is a great feature of DataLad (explained [here](https://handbook.datalad.org/en/latest/basics/101-106-nesting.html) in more detail) and used throughout all of the datasets we share here.

### Download data

Each dataset is not only associated with a dedicated GitHub repository but also a separate GIN repository that contains the actual file contents.
All project-relevant GIN repositories can be found at <https://gin.g-node.org/lnnrtwttkhn> and are associated with a DOI, that allows to cite them separately from the paper and clearly indicate which dataset has been used.

## About the paper

> Wittkuhn, L. and Schuck, N. W. (2020). Dynamics of fMRI patterns reflect sub-second activation sequences and reveal replay in human visual cortex. *Nature Communications*.

A preprint of the paper is available on bioRxiv:

> Wittkuhn, L. and Schuck, N. W. (2020). Faster than thought: Detecting sub-second activation sequences with sequential fMRI pattern analysis. *bioRxiv*. [doi: 10.1101/2020.02.15.950667](http://dx.doi.org/10.1101/2020.02.15.950667)

## About the authors

**Lennart Wittkuhn** is a PhD candidate in the [Max Planck Research Group "Neural and Computational Basis of Learning, Decision Making and Memory" - NeuroCode](https://www.mpib-berlin.mpg.de/research/research-groups/mprg-neurocode) at the [Max Planck Institute for Human Development Berlin, Germany](https://www.mpib-berlin.mpg.de/en).
He is also a pre-doctoral fellow at the [International Max Planck Research School on Computational Methods in Psychiatry and Ageing Research (IMPRS COMP2PSYCH)](https://www.mps-ucl-centre.mpg.de/comp2psych), part of the [Max Planck UCL Centre for Computational Psychiatry and Ageing Research](https://www.mps-ucl-centre.mpg.de/) and a [ReproNim/INCF 2020-2021 Training Fellow](https://www.repronim.org/fellowship.html).
Write Lennart at [wittkuhn@mpib-berlin.mpg.de](mailto:wittkuhn@mpib-berlin.mpg.de), follow him on [Twitter](https://twitter.com/lnnrtwttkhn) or [GitHub](https://github.com/lnnrtwttkhn) and visit [https://lennartwittkuhn.com/](https://lennartwittkuhn.com/) for further information.

**Nicolas W. Schuck** is the principal investigator of the [Max Planck Research Group "Neural and Computational Basis of Learning, Decision Making and Memory" - NeuroCode](https://www.mpib-berlin.mpg.de/research/research-groups/mprg-neurocode) at the [Max Planck Institute for Human Development Berlin, Germany](https://www.mpib-berlin.mpg.de/en) and faculty member of the [Max Planck UCL Centre for Computational Psychiatry and Ageing Research](https://www.mps-ucl-centre.mpg.de/).
Write Nico at [schuck@mpib-berlin.mpg.de](mailto:schuck@mpib-berlin.mpg.de), follow him on [Twitter](https://twitter.com/nico_schuck) and visit [schucklab.gitlab.io](https://schucklab.gitlab.io/) for further information about Nico and the lab.

## Acknowledgements

This work was funded by a research group grant awarded to Nicolas W. Schuck by the Max Planck Society (M.TN.A.BILD0004).
We also acknowledge financial support by the Max Planck Institute for Human Development.

We thank *Eran Eldar*, *Sam Hall-McMaster* and *Ondrej Zika* for helpful comments on a previous version of this manuscript, *Gregor Caregnato* for help with participant recruitment and data collection, *Anika Löwe*, *Sonali Beckmann* and *Nadine Taube* for assistance with MRI data acquisition, *Lion Schulz* for help with behavioral data analysis, *Michael Krause* for support with cluster computing and all participants for their participation.

Lennart Wittkuhn is a pre-doctoral fellow of the International Max Planck Research School on Computational Methods in Psychiatry and Ageing Research (IMPRS COMP2PSYCH).
The participating institutions are the Max Planck Institute for Human Development, Berlin, Germany, and University College London, London, UK.
For more information, see [https://www.mps-ucl-centre.mpg.de/en/comp2psych](https://www.mps-ucl-centre.mpg.de/en/comp2psych).

## License information

**1. Please cite our paper!**

First, if you use any of the code or data published with the paper Wittkuhn & Schuck, 2020, *Nature Communications*, please be so kind to cite the paper:

> Wittkuhn, L. and Schuck, N. W. (2020). Dynamics of fMRI patterns reflect sub-second activation sequences and reveal replay in human visual cortex. *Nature Communications*.

**2. Please cite our code and data!**

Each of the datasets published on GIN is associated with a unique DOI.
If you use a particular dataset (e.g., the BIDS-structured MRI data), please cite the related DOI in addition to our paper.

**3. If you download any data, please complete our Data User Agreement!**

If you download any of the published data, please [complete our Data User Agreeement (DUA)](https://survey3.gwdg.de/index.php?r=survey/index&sid=249576&lang=en).
The Data User Agreement (DUA) we use for this study, was taken from the [Open Brain Consent project](https://open-brain-consent.readthedocs.io/en/stable/gdpr/data_user_agreement.html), distributed under [Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/).

**4. Data is licensed under CC BY-NC-SA 4.0**

If not stated otherwise, all data is shared under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

**5. Code is licensed under MIT**

If not stated otherwise, all custom code written by the authors (Lennart Wittkuhn and Nicolas W. Schuck) is licensed under the [MIT license](https://opensource.org/licenses/MIT).

**6. Please be mindful!**

Please note, that subdatasets / submodules of this repository may have different licenses.

Further, code and data from third-parties, e.g., software like containers (e.g., fMRIPrep, MRIQC, etc.) may have different licenses.

All in all, properly licensing mixed code and data from different sources is tough, so please be mindful, respect and credit the work of others and ask if you are unsure about the appropriate referencing of sources.
That being said, please also reach out to us if you are a licensor and noticed that we did not properly credited your work!
Thanks!

# Results

## Overview

The following sections contain the main results reported in the paper.
The results are shown next to the `R` code that produced them.
Specifically, all code is written in `RMarkdown` which allows to combine analysis code with regular text.
The rendered `RMarkdown` files you see here, were generated automatically based on data stored in the relevant data sets listed above using continuous integration in [the associated GitLab repository](https://git.mpib-berlin.mpg.de/wittkuhn/highspeed).
This means that upon any change to the analysis code, the relevant data is retrieved from the relevant sub-directories and all analyses are re-generated.

Please see the following sections for details on the computational environment and the continuous integration pipeline.

#### Computational environment

Below we show the version information about R, the operating system (OS) and attached or loaded R packages using [`sessionInfo()`](https://www.rdocumentation.org/packages/utils/versions/3.6.2/topics/sessionInfo)

```{r, echo=FALSE}
sessionInfo()
```

#### Continuous integration

Below we show the continuous integration script that was used to automatically generate this project website.
This is made possible by using thee great tools [DataLad](https://www.datalad.org/) and [bookdown](https://bookdown.org/home/).

```{bash, echo=TRUE, code=readLines(here::here(".gitlab-ci.yml")), eval=FALSE}
```
